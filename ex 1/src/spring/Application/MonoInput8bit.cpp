#include <spring\Application\MonoInput8bit.h>
#include <iostream>
#include <qendian.h>
#include <qaudiodeviceinfo.h>
namespace Spring
{
	MonoInput8bit::MonoInput8bit(int sampleRate,double displayTime)
	{
		mAudioFormat.setSampleRate(sampleRate);
		mAudioFormat.setChannelCount(1);
		mAudioFormat.setSampleSize(8);
		mAudioFormat.setSampleType(QAudioFormat::UnSignedInt);
		mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
		mAudioFormat.setCodec("audio/pcm");
		mMaxAmplitude = 32767;
		mDisplayTime = displayTime;
	}
	qint64 MonoInput8bit::readData(char* data,qint64 maxlen) 
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}
	qint64 MonoInput8bit::writeData(const char* data, qint64 len)
	{
		//const auto* ptr = reinterpret_cast<const unsigned char*>(data);
		
		const auto* ptr = data;

		int channelBytes = mAudioFormat.sampleSize() / 8;
		double dataLength = mAudioFormat.sampleRate()*mDisplayTime;
		
		for (auto i = 0; i < len / channelBytes; ++i)
		{
			qint32 value = 0;
			value = qFromLittleEndian<qint16>(ptr);
			auto level = float(value)*(5. / mMaxAmplitude);
			mSamples.push_back(level);
			ptr += channelBytes;
		}

		if (mSamples.size() > dataLength)
			mSamples.remove(0, mSamples.size() - dataLength);

		std::cout << len << std::endl;
		return len;
	}
	QAudioFormat MonoInput8bit::getAudioFormat()
	{
		return mAudioFormat;
	}
	QVector<double> MonoInput8bit::vecGetData() const
	{
		return mSamples;
	}
}