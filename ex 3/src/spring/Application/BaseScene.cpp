#include "..\..\..\include\spring\Application\BaseScene.h"
#include <stdlib.h>
#include <time.h>
#include "aquila\transform\FftFactory.h"
namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
		timer = new QTimer(this);
		QObject::connect(timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));
		//unsigned int refreshRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("RefreshRate")->second);
		//timer->setInterval(1000/refreshRate);
		timer->setInterval(1000);
		
	}

	void BaseScene::createScene()
	{

		createGUI();
		
		QObject::connect(pushButtonBack, SIGNAL(released()), this, SLOT(buttonBack()));
		QObject::connect(pushButtonStart, SIGNAL(released()), this, SLOT(mf_StartTimer()));
		QObject::connect(pushButtonStop, SIGNAL(released()), this, SLOT(mf_StopTimer()));
		
	}

	void BaseScene::release()
	{
		delete timer;
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		pushButtonStop = new QPushButton(centralWidget);
		pushButtonStop->setObjectName(QStringLiteral("pushButtonStop"));

		gridLayout->addWidget(pushButtonStop, 1, 2, 1, 1);

		pushButtonBack = new QPushButton(centralWidget);
		pushButtonBack->setObjectName(QStringLiteral("pushButtonBack"));
		pushButtonBack->setShortcut(QStringLiteral("Shift+B"));

		gridLayout->addWidget(pushButtonBack, 1, 3, 1, 1);

		pushButtonStart = new QPushButton(centralWidget);
		pushButtonStart->setObjectName(QStringLiteral("pushButtonStart"));
		pushButtonStart->setShortcut(QStringLiteral("Shift+S"));

		gridLayout->addWidget(pushButtonStart, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));
		customPlot->addGraph();
		gridLayout->addWidget(customPlot, 0, 0, 1, 4);

		plotFFT = new QCustomPlot(centralWidget);
		plotFFT->setObjectName(QStringLiteral("plot"));
		gridLayout->addWidget(plotFFT, 2, 0, 1, 4);


		m_uMainWindow.get()->setCentralWidget(centralWidget);

		m_uMainWindow.get()->setWindowTitle("DisplayWindow");
		pushButtonStop->setText("Sto&p");
		pushButtonStop->setShortcut(QApplication::translate("MainWindow", "Alt+P", Q_NULLPTR));
		pushButtonBack->setText("&Back");
		pushButtonBack->setShortcut(QApplication::translate("MainWindow", "Alt+B", Q_NULLPTR));
		pushButtonStart->setText("&Start");
		pushButtonStart->setShortcut(QApplication::translate("MainWindow", "Alt+S", Q_NULLPTR));

		xAxis.clear();
		for (int i = 0; i < 25600 * 0.1; i++)
		{
			xAxis.push_back((double)i*1.0 / 25600);
		}
	}
	void BaseScene::buttonBack()
	{

		std::string title = "DisplayWindow";
		m_uMainWindow->setWindowTitle(QString(title.c_str()));

		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
	void BaseScene::mf_PlotRandom()
	{
		//X.clear();
		//Y.clear();
		srand(time(NULL));
		
		Y = mMonoInput->vecGetData();
		
		xAxis.resize(Y.size());
		customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
		customPlot->graph(0)->setData(xAxis, Y);
		customPlot->rescaleAxes();
		customPlot->replot();
	}
	void BaseScene::mf_CleanPlot()
	{
		customPlot->graph(0)->data()->clear();
		customPlot->replot();
	}
	void BaseScene::mf_StartTimer()
	{
		if (!timer->isActive())
		{
			timer->start();
			mMonoInput = new MonoInput(25600,0.10);
			mMonoInput->open(QIODevice::WriteOnly);

			mAudioInput = new QAudioInput(mMonoInput->getAudioFormat());
			mAudioInput->start(mMonoInput);
		}
		
	}

	
	void BaseScene::mf_StopTimer()
	{
		if (timer->isActive())
		{
			timer->stop();
			mf_CleanPlot();
			mAudioInput->stop();
			mMonoInput->close();
		}
		
	}
	void BaseScene::plotSignal()
	{
		int FFTsize = 128;
		double frequencyRes = 44100 / FFTsize;
		auto fft = Aquila::FftFactory::getFft(FFTsize);
		auto spectrum = fft->fft(Y.toStdVector().data());
		QVector<double> amplitude, frequency;
		int index = 0;
		for each(auto item in spectrum)
		{
			amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
			frequency.push_back((index++)*frequencyRes);
		}
		plotFFT->addGraph();
		plotFFT->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
		plotFFT->graph(0)->setData(frequency, amplitude);
		plotFFT->xAxis->setRange(20, 20000);
		plotFFT->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	}
}
