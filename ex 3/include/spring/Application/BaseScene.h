#pragma once
#include <spring\Framework\IScene.h>
#include <qcustomplot.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <qtimer.h>
#include <spring\Application\MonoInput.h>
#include <qaudioinput.h>
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		void plotSignal();

		~BaseScene();
		
	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QPushButton *pushButtonStop;
		QPushButton *pushButtonBack;
		QPushButton *pushButtonStart;
		QSpacerItem *horizontalSpacer;
		QCustomPlot *customPlot;
		QCustomPlot *plotFFT;
		QVector<double>X;
		
		
		QTimer *timer;

		MonoInput *mMonoInput;
		QAudioInput *mAudioInput;
		QVector<double>xAxis;
		QVector<double>Y; //e yAxis

	private slots:
		void buttonBack();
		void mf_PlotRandom();
		void mf_CleanPlot();
		void mf_StartTimer();
		void mf_StopTimer();
	};

}
