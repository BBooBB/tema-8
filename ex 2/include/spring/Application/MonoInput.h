#pragma once
#include <qaudioformat.h>
#include <qiodevice.h>
#include <qvector.h>
namespace Spring
{
	class MonoInput:public QIODevice
	{
	public:
		MonoInput(int,double);
		qint64 readData(char*, qint64) override;
		qint64 writeData(const char*, qint64);
		QAudioFormat getAudioFormat();
		QVector<double> vecGetData() const;
		MonoInput();

	private:
		QAudioFormat mAudioFormat;
		QVector<double> mSamples;
		qint32 mMaxAmplitude;
		double mDisplayTime;
	};

}
